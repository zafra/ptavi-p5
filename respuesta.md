### Respuestas ejercicio 3


* ¿Cuántos paquetes componen la captura?

      1050 paquetes
* ¿Cuánto tiempo dura la captura?

      10.521629838 segundos
* ¿Qué IP tiene la máquina donde se ha efectuado la captura?

      192.168.1.116
* ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes? 

      Es una IP privada. Está dentro del rango de direcciones del bloque 192.168.0.0/16,
      establecido para el uso de comunicaciones locales dentro de una red privada 
      en el protocolo IPv4, el usado en este caso.
* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
  
      El principal protocolo de nivel de aplicación es el protocolo RTP con un 98,2% del total de paquetes,
      lo que se corresponde a 1031 paquetes. El segundo es SIP con un 1,2% del total de paquetes(13 paquetes).
      No habría tercero porque  ICMP es un protocolo de nivel de red y el resto son datos.
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?

      El protocolo Ethernet, que es a través del cual viaja toda la trama.
      IPv4, que es el protocolo de red que define la dirección de nombres.
      ICMP, que es un protocolo de red.
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?

      RTP, con 177332 bytes enviados y una tasa de 16,75KBytes por segundo
* ¿En qué segundos tienen lugar los dos primeros envíos SIP?
      
      En los segundos 0.000012938 y 0.063293012
* Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
      
      Se empiezan a enviar a partir del paquete número 6 
* Los paquetes RTP, ¿cada cuánto se envían?
 
      Se envían de forma continuada con una diferencia de unas centésimas o milésimas de segundo


### Respuestas ejercicio 4

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.



* ¿De qué protocolo de nivel de aplicación son?

      Son paquetes SIP, el primero de ellos se apoya además en el protocolo SDP.
* ¿Cuál es la dirección IP de la máquina "Linphone"?

      192.168.1.116
* ¿Cuál es la dirección IP de la máquina "Servidor"?

      212.79.111.155
* ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?

      El cliente(user-agent) manda un mensaje de tipo INVITE al servidor para poder establecer una sesión. 
* ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
      
      El servidor responde al mensaje INVITE con un código 100, 
      para informar que se está tratando la información(trying). 

Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?

      Son paquetes SIP, el primero de ellos se apoya además en el protocolo SDP.
* ¿Entre qué máquinas se envía cada trama?

      La primera trama se envía desde el servidor, con dirección IP 212.79.111.155, 
      al cliente (user-agent), con dirección IP 192.168.1.116. 
      En la segunda trama cambia la comunicacón es al revés, del cliente al servidor. 
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
      
      Después de que el servidor haya recibido el mensaje INVITE del user-agent,
      y haber mandado una respuesta con el código 100, informando de que se estaba tratando la información,
      el servidor manda otro mensaje con el código 200 OK, indicando que la petición ha sido aceptada.
      
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?

      El cliente manda un mensaje ACK para confirmar el establecimiento de la sesión.

### Respuestas ejercicio 5

Después de la trama 250, busca la primera trama SIP.

* ¿Qué número de trama es?

      La 1042
* ¿De qué máquina a qué máquina va? 

      Se envía desde el cliente(user-agent), con dirección IP 192.168.1.116,
      al servidor, con dirección IP 212.79.111.155
* ¿Para qué sirve?

      El cliente manda un mensaje BYE al servidor para terminar con la sesión.
* ¿Puedes localizar en ella qué versión de Linphone se está usando?
       
      Sí, es Linphone Desktop/4.3.2

### Respuestas ejercicio 6

Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

* ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
      
      sip:music@sip.iptel.org
* ¿Qué instrucciones SIP entiende el UA?
      
      INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE.
      
* ¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?
      
      La cabecera Content-type que es de tipo aplication/SDP.
* ¿Cuál es el nombre de la sesión SIP?

      Talk.

### Respuestas ejercicio 7

* ¿Qué trama lleva esta propuesta?
 
      Las tramas 2, 139 y 193
* ¿Qué indica el `7078`?

      Es el puerto UDP usado por el cliente para el transporte de los datos,
      en este caso audio.
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?

      Todos los paquetes RTP de audio que envíe el cliente tendrán este puerto.
* ¿Qué paquetes son esos?

      2, 139 y 193

En la respuesta a esta propuesta vemos un campo `m` con un valor que empieza por `audio XXX`.

* ¿Qué trama lleva esta respuesta?

      Las tramas 4, 236 y 243
* ¿Qué valor es el `XXX`?

      29448 
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?

      Todos los paquetes RTP de audio que envíe el servidor tendrán este puerto. 
* ¿Qué paquetes son esos?

      4, 236 y 243

### Respuestas ejercicio 8

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

* ¿De qué máquina a qué máquina va?

      De la máquina con dirección IP 192.168.1.116 a la máquina con dirección IP 212.79.111.155
* ¿Qué tipo de datos transporta?

      Transporta audio codificado con el códec ITU-T G.711 PCMU.
* ¿Qué tamaño tiene?

      1712 bits(214 bytes).
* ¿Cuántos bits van en la "carga de pago" (payload)

      172 bytes.
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?

      Aproximadamente cada dos centésimas de segundo, 0,02 segundos.

### Respuestas ejercicio 9

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?

      Hay dos flujos, uno del user-agent al servidor y otro del servidor al user-agent. 
      Debido a que estamos realizando una llamada de VoIP, tendrá que haber flujo en ambos sentidos para que se produzca la comunicación.
* ¿Cuántos paquetes se pierden?

      No se ha perdido ningún paquete durante la llamada 
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?

      30.726700 ms
* ¿Qué es lo que significa el valor de delta?

      Es la diferencia de tiempo que hay entre el paquete actual y el anterior.
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?

      En el flujo del servidor con dirección IP 212.79.111.155 al user-agent con dirección IP 192.168.1.116
      Mean Jitter = 2.995904 ms y  Max Jitter = 7.110079 ms.
* ¿Qué significan esos valores?

      Representa la variabilidad del retardo de envío de los paquetes. 
      Max jitter indica que ha habido un paquete cuya fluctuación ha sido de 7.110079ms,
      que es la más alta.
      Mean Jitter es el valor medio de jitter que tienen los paquetes de ese flujo.

Vamos a ver ahora los valores de una trama concreto, la númro 27. Vamos a analizarla en opción `Telephony`, `RTP`, `RTP Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?

      El delta vale 0.000164 ms y el jitter 3.087182 ms.
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?

      Sí, por el valor skew que es muy negativo.
* El "skew" es negativo, ¿qué quiere decir eso?
   
      Que el paquete ha llegado con retraso respecto al reloj de la comunicación.

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?

      Una señal de audio de voz.
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?

      La calidad de la señal de audio ha empeorado.
* ¿A qué se debe la diferencia?
    
      Con un jitter buffer tan pequeño no se puede asegurar la calidad de la señal,
      ya que se perderán paquetes.

### Respuestas ejercicio 10

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` selecciona el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?

      10 segundos.
Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.
* ¿En qué segundo se recibe el último OK que marca el final de la llamada?

       En el segundo 10.521629838.
  
Ahora, selecciona los dos streams, y pulsa sobre `Play Sterams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?

      El 0xd2db8b4 desde el user-agent al servidor y el 0x5c44a34b desde el servidor al user-agent.
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?

      Se envían 514 paquetes.
* ¿Cuál es la frecuencia de muestreo del audio?

      La frecuencia de muestreo es de 8000Hz(8KHz).
* ¿Qué formato se usa para los paquetes de audio (payload)?

      El g711U, haciendo referencia al códec G.711

### Respuestas ejercicio 11

Desde LinPhone, créate una cuenta SIP.  A continuación:

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 10 segundos de duración. Comienza a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Asegura (usando los filtros antes de guardarla) que en la captura haya solo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos RTP tiene esta captura?

      3 flujos.
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?

      Del flujo que va de la máquina con dirección IP 212.79.111.155 a la máquina con dirección Ip 212.128.254.134:
            * Delta ---> 35.879824 ms 
            * Max Jitter ---> 3.303321 ms
            * Mean Jitter ---> 0.910889 ms
      
      Del flujo que va de la máquina con dirección IP 212.128.254.134 a la máquina con dirección Ip 212.79.111.155:
            * Delta ---> 40.783104 ms
            * Max Jitter ---> 10.451400 ms
            * Mean Jitter ---> 5.748463 ms

      Del flujo que va de la máquina con dirección IP 212.128.254.134(el puerto es diferente al de la máquina con la misma ip) a la máquina con dirección Ip 212.79.111.155:
            * Delta ---> 30.121204 ms
            * Max Jitter ---> 3.071135 ms
            * Mean Jitter ---> 1.810788 ms